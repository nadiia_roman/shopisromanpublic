package com.shop.lab.controlers;

//import com.shop.lab.models.Order;
import com.shop.lab.models.Product;
import com.shop.lab.models.Seller;
import com.shop.lab.models.Shopper;
//import com.shop.lab.repo.OrderRepository;
import com.shop.lab.repo.ProductRepository;
import com.shop.lab.repo.SellerRepository;
import com.shop.lab.repo.ShopperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
//import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainControler {

//    @Autowired
//    private OrderRepository orderRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private SellerRepository sellerRepository;

    @Autowired
    private ShopperRepository shopperRepository;

    @GetMapping("/")
    public String home(Model model) {
        model.addAttribute("title", "Main page");
        return "home";
    }

    @GetMapping("/login")
    public String login(Model model) {
        model.addAttribute("title", "Main page");
        return "login";
    }

    @GetMapping("/seller")
    public String sellers(Model model) {
        List<Seller> sellers = sellerRepository.findAll();
        model.addAttribute("seller", sellers);
        return "seller";
    }

    @GetMapping("/shopper")
    public String shoppers(Model model) {
        List<Shopper> shoppers = shopperRepository.findAll();
        model.addAttribute("shopper", shoppers);
        return "shopper";
    }

    @GetMapping("/product")
    public String products(Model model) {
        List<Product> products = productRepository.findAll();
        model.addAttribute("product", products);
        return "product";
    }

//    @GetMapping("/order")
//    public String orders(Model model) {
//        List<Order> orders = orderRepository.findAll();
//        model.addAttribute("order", orders);
//        return "order";
//    }


}
