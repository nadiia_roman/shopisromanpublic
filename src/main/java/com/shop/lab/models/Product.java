package com.shop.lab.models;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "product")
public class Product {
    @Id
    @Column(name = "ID")
    private Long productID;

    @Column(name = "productName")
    private String productName;

    @Column(name = "productDescription")
    private String productDescription;

    @Column(name = "productWeight")
    private Long productWeight;

//    @OneToMany(orphanRemoval = true, cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
//   // @JoinColumn(name = "candidacy_id", nullable = false)
//    private List<Order> productOrders = new ArrayList<>();

    public Product(Long productID, String productName, String productDescription, Long productWeight /*, List<Order> productOrders*/) {
        this.productID = productID;
        this.productName = productName;
        this.productDescription = productDescription;
        this.productWeight = productWeight;
        //this.productOrders = productOrders;
    }

    public Product() {
    }

    public Long getProductID() {
        return productID;
    }

    public void setProductID(Long productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public Long getProductWeight() {
        return productWeight;
    }

    public void setProductWeight(Long productWeight) {
        this.productWeight = productWeight;
    }

//    public List<Order> getProductOrders() {
//        return productOrders;
//    }
//
//    public void setProductOrders(List<Order> productOrders) {
//        this.productOrders = productOrders;
//    }
}
