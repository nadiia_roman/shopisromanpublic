package com.shop.lab.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "shopper")
public class Shopper {
    @Id
    @Column(name = "ID")
    private Long shopperID;

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

//    @OneToMany(orphanRemoval = true, cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
//   // @JoinColumn(name = "candidacy_id", nullable = false)
//    private List<Order> shopperOrders = new ArrayList<>();

    public Shopper(Long shopperID, String firstName, String lastName, String email, String password/*, List<Order> shopperOrders*/) {
        this.shopperID = shopperID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        //this.shopperOrders = shopperOrders;
    }

    public Shopper() {
    }

    public Long getShopperID() {
        return shopperID;
    }

    public void setShopperID(Long shopperID) {
        this.shopperID = shopperID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

//    public List<Order> getShopperOrders() {
//        return shopperOrders;
//    }
//
//    public void setShopperOrders(List<Order> shopperOrders) {
//        this.shopperOrders = shopperOrders;
//    }
}
