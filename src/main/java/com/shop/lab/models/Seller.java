package com.shop.lab.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "seller")
public class Seller {
    @Id
    @Column(name = "ID")
    private Long sellerID;

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "salary")
    private Long salary;

    @Column(name = "workHours")
    private Long workHours;

//    @OneToMany(orphanRemoval = true, cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
//   // @JoinColumn(name = "candidacy_id", nullable = false)
//    private List<Order> sellerOrders = new ArrayList<>();

    public Seller(Long sellerID, String firstName, String lastName, Long salary, Long workHours /*List<Order> sellerOrders*/) {
        this.sellerID = sellerID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
        this.workHours = workHours;
        //this.sellerOrders = sellerOrders;
    }

    public Seller() {
    }

    public Long getSellerID() {
        return sellerID;
    }

    public void setSellerID(Long sellerID) {
        this.sellerID = sellerID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getSalary() {
        return salary;
    }

    public void setSalary(Long salary) {
        this.salary = salary;
    }

    public Long getWorkHours() {
        return workHours;
    }

    public void setWorkHours(Long workHours) {
        this.workHours = workHours;
    }

//    public List<Order> getSellerOrders() {
//        return sellerOrders;
//    }
//
//    public void setSellerOrders(List<Order> sellerOrders) {
//        this.sellerOrders = sellerOrders;
//    }
}
